package com.packci.app;

public class App 
{
    public static void main( String[] args )
    {
        int[] numbers = {
                1, 2, 3, 4, 5
        };
        int avg = average(numbers);
        System.out.println(avg);
    }

    public static int average(int[] numbers) {
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        return sum;
    }
}
